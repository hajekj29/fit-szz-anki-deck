# FIT SZZ Anki deck
## Obsah
* [Progress](#progress)
* [Co je to Anki?](#co-je-to-anki%3F)
* [Motivace](#motivace)
* [Struktura repozitáře](#struktura-repozitáře)
* [Importování decků](#importování-decků)

## Progress
Viz [Milestones](https://gitlab.fit.cvut.cz/hajekj29/fit-szz-anki-deck/milestones).

| NI-SPOL                | NI-PB                  | NI-ZI                  |
|------------------------|------------------------|------------------------|
| <tt>NI-KOP 0/5 ❌</tt> | <tt>NI-AIB 2/2 ✅</tt> | <tt>NI-ADM 0/3 ❌</tt> |
| <tt>NI-MPI 5/5 ✅</tt> | <tt>NI-HWB 2/3 ❌</tt> | <tt>NI-BML 0/3 ❌</tt> |
| <tt>NI-VSM 0/5 ❌</tt> | <tt>NI-KRY 2/3 ❌</tt> | <tt>NI-MVI 0/3 ❌</tt> |
| <tt>NI-PDP 5/5 ✅</tt> | <tt>NI-MKY 1/3 ❌</tt> | <tt>NI-PDD 0/3 ❌</tt> |
|                        | <tt>NI-REV 4/4 ✅</tt> | <tt>NI-PON 0/3 ❌</tt> |
|                        | <tt>NI-SBF 0/2 ❌</tt> | <tt>NI-SCR 0/3 ❌</tt> |
|                        | <tt>NI-SIB 0/4 ❌</tt> | <tt>NI-UMI 0/3 ❌</tt> |

## Co je to Anki?
Anki je software, který pomáhá zapamatovat si různé informace, založený na systému opakování na základě křivek zapomínání. Při výuce uživateli postupně zobrazuje předem vytvořené karty (otázky), které uživatel hodnotí podle toho, jak dobře si danou kartu (odpověď) pamatuje. Interval, ve kterém se stejná karta objeví znovu se postupně zvyšuje, a to v závislosti na hodnocení uživatele. Algoritmus programu nabízí uživateli kartičky ke zopakování v momentě, kdy má informace na kartičkách zapomenout, právě tehdy je nejlepší opakovat, jelikož dochází k největšímu posílení paměťové stopy. Program dokáže nejvhodnější moment pro opakování odhadnout právě na základě odpovědí při předchozích učebních epizodách. [[1]](https://cs.wikipedia.org/wiki/Anki)

## Motivace
Jelikož mě nebavilo učit se na bakalářské státnice až těsně před nimi pod velkým stresem a Anki se mi následně osvědčilo hned v prvním semestru magistra, rozhodl jsem se vytvořit Anki deck, se kterým se budu průběžně připravovat na magisterské státnice.

Z vlastní zkušenosti tato průběžná příprava stojí relativně málo úsilí a do budoucna výrazně zjednoduší (či v některých případech zcela eliminuje) přípravu na zkoušku. Ze začátku může být učení trochu intenzivnější, což se dá obejít snížením denních limitů decku, pokud se začnete učit s dostatečným předstihem. Následně se review intervaly kartiček, které umíte, mohou zvednout až na několik týdnů nebo měsíců, tj. neučíte se zbytečně často to, co stejně zrovna umíte.

Více informací např. v přednášce [Student 2.0: Nauč se učit se](https://youtu.be/dAVMQESBBjg?t=2825) od Elišky Šestákové.

## Struktura repozitáře
Jelikož jsem nenašel jednoduchý způsob, jak Anki decky hezky verzovat, rozhodl jsem se vytvářet samostatný deck pro každý okruh. Název decku je vždy ve formátu

```
FIT SZZ::<soubor okruhů>::<kód předmětu>::<kód okruhu> <název okruhu>
```

Například

`FIT SZZ::NI-SPOL::NI-KOP::NI-SPOL-11 Význam tříd NP a NPH pro praktické výpočty.`

Samotný repozitář tuto strukturu reflektuje. Jednotlivé decky jsou uloženy v

```
FIT SZZ/<soubor okruhů>/<kód předmětu>/<kód okruhu>.apkg
```

Například
```
FIT SZZ
├── NI-SPOL
│   ├── NI-MPI
│   │   ├── NI-SPOL-1.apkg
│   │   └── NI-SPOL-2.apkg
│   └── NI-KOP
│       ├── NI-SPOL-11.apkg
│       └── NI-SPOL-12.apkg
└── NI-PB
    ├── NI-REV
    │   ├── NI-PB-12.apkg
    │   └── NI-PB-13.apkg
    └── NI-SBF
        ├── NI-PB-20.apkg
        └── NI-PB-21.apkg
```

## Importování decků
V Anki se importuje (`File->Import...`) soubor po souboru takže abyste se neuklikali, doporučuji použít addon [MultiDeckImporter](https://ankiweb.net/shared/info/1263172192) (`Tools->Add-ons->Get Add-ons` a zadat kód `1263172192`), se kterým lze importovat například celou `FIT SZZ` složku naráz (`Tools->Import From Folder...`).

**Pozor!** Tento plugin v současné verzi (2020-08-20) by default přídává deckům do názvu prefix podle cesty, čili pro naše účely je potřeba si ho trochu upravit, aby před názvy decků nic nepřidával. Přes (`Tools->Add-ons, označit MultiDeckImporter, View Files`) najdeme `__init__.py` tohoto addonu a zakomentujeme několik řádek.

```
51  # We're going to add a menu item below. First we want to create a function to
52  # be called when the menu item is activated.
53  def importFromFolder():
54      mw.importDialog = QFileDialog()
55      mw.importDialog.setFileMode(QFileDialog.Directory)
56
57      if mw.importDialog.exec_():
58          fileNames = mw.importDialog.selectedFiles()
59
60          if len(fileNames) > 0:
61              rootDir = fileNames[0]
62              baseName = os.path.basename(rootDir)
63              prefixLen = len(rootDir) - len(baseName)
64              for root, subDirs, files in os.walk(rootDir):
65                  for f in files:
66                      if f.endswith(".apkg"):
67                          # Get name for deck
68  # tuhle ->              deckName = "::".join(root[prefixLen:].split(os.sep)) + "::" + f[0: len(f) - 5]
69
70  # tuhle ->              did = mw.col.decks.id(deckName)
71  # tuhle ->              mw.col.decks.save(mw.col.decks.get(did))
72                          importer = CustomImporter(mw.col, os.path.join(root, f))
73  # a tuhle ->            importer.deckPrefix = deckName
74                          importer.run()
75
76              mw.reset()
```
